﻿using System.IO;
using System.Net;

namespace DownloaderApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = "https://speed.hetzner.de/100MB.bin",
            savePath = args[0],
            path = savePath.Substring(0, savePath.LastIndexOf(@"\")),
            file = savePath.Substring(savePath.LastIndexOf(@"\"));

            if (File.Exists(savePath))
            {
                File.Delete(savePath);
            }

            try
            {
                var directory = new DirectoryInfo(path);
                if (!directory.Exists)
                {
                    directory.Create();
                }

                var client = new WebClient();
                client.DownloadFile(url, savePath);
            }
            catch
            {
                return;
            }
        }
    }
}
