﻿using System.IO;
using System.Linq;
using System.Numerics;

namespace CalculatorApp
{
    public static class Program
    {
        static void Main(string[] args)
        {
            string octalString = "16304103460644701340432043410021040424210140423204";

            try
            {
                var path = @"C:\ExpressionResult\";

                var directory = new DirectoryInfo(path);
                if (!directory.Exists)
                {
                    directory.Create();
                }

                path += "result.txt";

                using (var stream = new StreamWriter(path, false))
                {
                    var result = octalString.Aggregate(new BigInteger(), (b, c) => (b * 8) + c - '0');
                    stream.WriteLine(result);
                }
            }
            catch
            {
                return;
            }
        }
    }
}
