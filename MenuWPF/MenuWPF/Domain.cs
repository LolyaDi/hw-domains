﻿using System;
using System.Threading.Tasks;

namespace MenuWPF
{
    public class Domain
    {
        private AppDomain _domain;

        public async Task ExecuteAssemblyAsync(string domainName, string assemblyName, string[] methodArguments)
        {
            var currentDomain = AppDomain.CurrentDomain;
            string assemblyPath = currentDomain.BaseDirectory + assemblyName;

            _domain = AppDomain.CreateDomain(domainName);

            await Task.Run(() => _domain.ExecuteAssembly(assemblyPath, methodArguments));

            AppDomain.Unload(_domain);
        }
    }
}
